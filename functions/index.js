// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
// https://firebase.google.com/docs/functions/get-started
const functions = require("firebase-functions");
const admin = require("firebase-admin");

const { WebhookClient } = require("dialogflow-fulfillment");
const { Card, Suggestion } = require("dialogflow-fulfillment");

admin.initializeApp();

process.env.DEBUG = "dialogflow:debug";

exports.dialogflowFirebaseFulfillment = functions.https.onRequest(
  (request, response) => {
    const agent = new WebhookClient({ request, response });
    console.log("Dialogflow request header:", JSON.stringify(request.headers));
    console.log("Dialogflow request body:", JSON.stringify(request.body));

    function welcome(agent) {
      agent.add("Hallo");
    }

    function fallback(agent) {
      agent.add("ik snap je niet");
    }

    function noWater(agent) {
      agent.add("Er is geen storing gemeld bij jou in de buurt");
    }

    let intentMap = new Map();
    intentMap.set("Default Welcome Intent", welcome);
    intentMap.set("Default Fallback Intent", fallback);
    intentMap.set("geen water", noWater);
  }
);

// exports.addMessage = functions.https.onRequest(async (req, res) => {
//   console.log("hallo");
//   console.log("wow");
// });
