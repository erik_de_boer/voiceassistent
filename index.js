const express = require("express");
const bodyParser = require("body-parser");

// https://firebase.google.com/docs/functions/get-started
const admin = require("firebase-admin");
const functions = require("firebase-functions");
const { WebhookClient } = require("dialogflow-fulfillment");
const dialogflow = require("dialogflow");

// const { dialogflow } = require("actions-on-google");

const app = express();

var serviceAccount = require("./cert.json");

/**
 * Configure app to use bodyParser()
 */
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// start with ngrok
app.post("/", (req, res) => {
  const agent = new WebhookClient({ req, res });
  // console.log(
  //   "Post :: Dialogflow request header:",
  //   JSON.stringify(req.headers)
  // );
  // console.log("Post :: Dialogflow request body:", req.body);
  console.log(req.body.queryResult.intent.displayName);

  res.send("<speak>sure?</speak>");
});

app.get("/", (req, res) => {
  console.log("Dialogflow request header:", JSON.stringify(req.headers));
  console.log("Dialogflow request body:", JSON.stringify(req.body));
  res.send("<speak>sure?</speak>");
});

const server = app.listen(8080, () => {
  console.log("initialize app");
  const host = server.address().address;
  const port = server.address().port;

  console.log(" -firebase");
  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
  });

  console.log(`chatbot listening at ${host}:${port} `);

  // createBucket();
  // runSample();
});

// async function createBucket() {
//   // await storage.createBucket(bucketName);
//   await storage.getBuckets(bucketName);
//   console.log(`Bucket ${bucketName} created.`);
// }

// /**
//  * Send a query to the dialogflow agent, and return the query result.
//  * @param {string} projectId The project to be used
//  */
// async function runSample(projectId = "waterstoring-rikstar") {
//   // A unique identifier for the given session
//   const sessionId = uuid.v4();

//   // Create a new session
//   const sessionClient = new dialogflow.SessionsClient();
//   const sessionPath = sessionClient.sessionPath(projectId, sessionId);

//   // The text query request.
//   const request = {
//     session: sessionPath,
//     queryInput: {
//       text: {
//         // The query to send to the dialogflow agent
//         text: "hello",
//         // The language used by the client (en-US)
//         languageCode: "en-US"
//       }
//     }
//   };

//   // Send request and log result
//   const responses = await sessionClient.detectIntent(request);
//   console.log("Detected intent");
//   const result = responses[0].queryResult;
//   console.log(`  Query: ${result.queryText}`);
//   console.log(`  Response: ${result.fulfillmentText}`);
//   if (result.intent) {
//     console.log(`  Intent: ${result.intent.displayName}`);
//   } else {
//     console.log(`  No intent matched.`);
//   }
// }
